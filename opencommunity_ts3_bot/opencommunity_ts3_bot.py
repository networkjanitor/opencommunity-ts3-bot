#!/usr/bin/python3

import ts3
import time
# fix strange project setup (for some reason pycharm does not find this import)
from persistency import session, Command
import re


class TS3InvalidCommand(ts3.TS3Error):
    def __init__(self, cmd=None):
        self.cmd = cmd

    def __str__(self):
        if self.cmd is not None:
            return 'The command "{}" is not a valid command.'.format(self.cmd)
        else:
            return 'The command could not be parsed from the message (probably no command in message)'


class TS3InsufficientPermissions(ts3.TS3Error):
    def __init__(self, client=None, action=None):
        self.client = client
        self.action = action

    def __str__(self):
        return 'The client "{}" is not allowed to perform the action "{}"'.format(self.client, self.action)


class TS3AuthenticationFailed(ts3.TS3Error):
    def __str__(self):
        return 'Authentication failed - maybe endpoint is not up yet?'


class OpenCommunityTS3Bot:
    def __init__(self, *, host="localhost", port="25639", apikey):
        self._ts3conn = ts3.query.TS3Connection(host, port)
        try:
            self._ts3conn.auth(apikey=apikey)
        except ts3.query.TS3QueryError as e:
            print("Authentication failed:", e.resp.error["msg"])
            raise TS3AuthenticationFailed()

    def _write_command_db(self, name, text):
        try:
            temp_cmd = self._read_command_db(name)
            temp_cmd.one()  # force exception if nothing was found
            temp_cmd.update({'text': text})
        except Exception as e:
            temp_cmd = Command(cmd=name, text=text)
            session.add(temp_cmd)
        session.commit()

    def _read_command_db(self, name):
        return session.query(Command).filter(Command.cmd == name)

    def get_command(self, name):
        try:
            result = self._read_command_db(name).one().text
            if result.strip() == '':
                raise TS3InvalidCommand()
            return result
        except:
            raise TS3InvalidCommand(cmd=name)

    def set_command(self, text, ts3_event):
        try:
            self.check_permissions(ts3_event[0])
        except TS3InsufficientPermissions:
            self.answer("Sorry {}, you are not permitted to do that. ' -'".format(ts3_event[0]['invokername']),
                        ts3_event)
            return

        space_index = text.find(' ')
        if space_index == -1:
            raise TS3InvalidCommand()

        cmd_name = text[:space_index]
        cmd_text = text[space_index + 1:]

        self._write_command_db(cmd_name, cmd_text)
        self.answer('Command "~{}" set, thanks {}! ^-^'.format(cmd_name, ts3_event[0]['invokername']), ts3_event)

    def unset_command(self, text, ts3_event):
        try:
            self.check_permissions(ts3_event[0])
        except TS3InsufficientPermissions:
            self.answer("Sorry {}, you are not permitted to do that. ' -'".format(ts3_event[0]['invokername']),
                        ts3_event)

        self._write_command_db(text.strip(), '')

    def answer(self, text, ts3_event):
        self._ts3conn.sendtextmessage(targetmode=ts3_event[0]['targetmode'], target=ts3_event[0]['invokerid'],
                                      msg=self.color_message(text))

    def color_message(self, text):
        return '[color={color}]{message}[/color]'.format(color='#1433b1', message=text)

    def parse_command(self, text):
        text = text.strip()

        if not self.is_command(text):
            raise TS3InvalidCommand()

        if ' ' in text:
            return text[1:text.find(' ')], text[text.find(' '):].strip()
        else:
            return text[1:], ''

    def check_permissions(self, ts3_event):
        # TODO: refactor, so that for each command there is a permission-check method
        # TODO: refactor "bot-owner" access to configuration file
        # Bot Owner
        if ts3_event['invokeruid'] == 'c7xjC/z2qBtueaxOSRtMKaSFgrg=' or \
                        ts3_event['invokeruid'] == 'z5TJ18g/dtkja4Xbrs9rOny4vl0=':
            return True

        client_id = ts3_event['invokerid']

        channel_group = self._ts3conn.clientvariable(client_id, "client_channel_group_id")[0]['client_channel_group_id']
        server_groups = self._ts3conn.clientvariable(client_id, "client_servergroups")[0]['client_servergroups']

        ## Control permissions here
        # TT Team {Commander, Trainee, Lieutnant}, Event Team
        if channel_group in ['10', '11', '15', '12']:
            return True

        # Community {Admin, Manager}, TS Guardian, Server Techy
        for sgroup in server_groups.split(','):
            if sgroup in ['6', '7', '13', '14']:
                return True

        raise TS3InsufficientPermissions()

    def is_command(self, text):
        text = text.strip()

        if text[0] != '~':
            return False

        return True

    def handle_event(self, ts3_event):
        # We dont want to handle our own messages
        # TODO: refactor to configuration file
        if ts3_event[0]['invokeruid'] == 'Jv864GKFtq4oEtgZjFRH/nYS7eQ=':
            return

        if ts3_event.event == 'notifytextmessage':
            self.handle_notifytextmessage(ts3_event)

    def is_tamobot_here(self):
        """
        Checks if one of Tamos bots is here (=> checks if there is a user whos name starts with "TamoBot")
        """
        clientlist = self._ts3conn.clientlist()
        whoami = self._ts3conn.whoami()

        for client in clientlist.parsed:
            if re.match("^TamoBot.*$", client['client_nickname']) and client['cid'] == whoami[0]['cid']:
                return True

        return False

    def handle_notifytextmessage(self, ts3_event):
        if not self.is_command(ts3_event[0]['msg']):  # Ignore all non-command messages
            return

        if ts3_event[0]['targetmode'] == '2':  # Look only for TamoBot if a command got sent to our channel
            if self.is_tamobot_here():
                return

        try:
            cmd, text = self.parse_command(ts3_event[0]['msg'])
            if cmd == 'set':
                self.set_command(text, ts3_event)
            elif cmd == 'unset':
                self.unset_command(text, ts3_event)
            else:
                self.answer(self.get_command(cmd), ts3_event)
        except TS3InvalidCommand as e:
            if ts3_event[0]['targetmode'] == '1':  # Only respond if it's a private conversation
                self.answer("Sorry, I have no idea what you said to me [malformed command] :(", ts3_event)

    def start_listening(self):
        self._ts3conn.clientnotifyregister(event="notifytextmessage", schandlerid=0)
        while True:
            self._ts3conn.send_keepalive()
            try:
                ts3_event = self._ts3conn.wait_for_event(timeout=540)
            except ts3.query.TS3TimeoutError:
                pass
            else:
                self.handle_event(ts3_event)

    def write_preset(self, name, text):
        try:
            self.get_command(name)
        except TS3InvalidCommand:
            self._write_command_db(name, text)


if __name__ == '__main__':
    # crude method of waiting until the teamspeak clientquery server has started
    # FIXME: should be wait for open telnet port, timeout after x seconds
    for x in range(0, 10):
        try:
            # TODO: refactor to configuration file
            bot = OpenCommunityTS3Bot(apikey="PS91-VAPK-MLYZ-I3GO-8574-C8IH")  # KTYJ-DNRH-1X5E-RM4O-E0IW-ATKI
        except TS3AuthenticationFailed:
            time.sleep(5)
        else:
            break

    # setup presets, if any of these commands already exist they will not be overwritten.
    # TODO: refactor to preset configuration file
    bot.write_preset('links',
                     '[url=http://www.theopencommunity.org/][b][color=#006666]Website![/color][/b][/url] | '
                     '[url=http://www.theopencommunity.org/feedback/][b][color=#009973]Feedback![/color][/b][/url] | '
                     '[url=http://www.theopencommunity.org/donate][b][color=#006666]Donate! \o/[/color][/b][/url] | '
                     '[url=http://www.theopencommunity.org/calendar/][b][color=#009973]Calendar![/color][/b][/url] | '
                     '[url=http://www.theopencommunity.org/forms/3-gw2-commander-sign-up/][b][color=#006666]Join the Team![/color][/b][/url] | '
                     '[url=http://www.theopencommunity.org/gw2/guilds/][b][color=#009973]Community Guilds![/color][/b][/url] | '
                     '[url=http://www.theopencommunity.org/topic/370-opencommunity-discord-verification-thread/][b][color=#006666]Discord![/color][/b][/url] | '
                     '[url=http://www.theopencommunity.org/gw2/raids/][b][color=#009973]Raids![/color][/b][/url] | '
                     '[url=http://www.theopencommunity.org/topic/1668-announcing-the-opencommunity-minecraft-server/][b][color=#006666]Minecraft![/color][/b][/url]')
    bot.write_preset('donate', '[url=http://www.theopencommunity.org/donate][color=#006666]Donate![/color][/url]')
    bot.write_preset('raids', '[url=http://www.theopencommunity.org/gw2/raids/][color=#006666]Raids![/color][/url]')
    bot.write_preset('feedback',
                     '[url=http://www.theopencommunity.org/feedback/][color=#006666]Feedback![/color][/url]')
    bot.write_preset('guides', '[color=#1433b1]Egg Blocking Guides:[/color] '
                               '[url=http://www.theopencommunity.org/topic/184-egg-blocking-triple-trouble-guide/]'
                               '[color=#cc00cc][b]Mesmer[/b][/color][color=#ffffff] [/color]'
                               '[color=#ff3333][b]Ele[/b][/color][color=#ffffff] [/color]'
                               '[color=#aa0000][b]Rev[/b][/color][color=#ffffff] [/color]'
                               '[color=#2f4f2f][b]Engi[/b][/color][/url][color=#ffffff] [/color]'
                               '[url=http://www.theopencommunity.org/topic/47-wurm-egg-blocking-guardian/]'
                               '[color=#0055aa][b]Guardian[/b][/color][/url]')
    bot.write_preset('info',
                     'Hi. I\'m TamoBots replacement as long as it isn\'t here. Only chat commands tho, no music. '
                     'See ~help for available commands. Contact: see description (in TS)')
    bot.write_preset('help', 'Available commands: \n'
                             '~links: OpenCommunity links (Guilds, Join the team, Raids, etc)\n'
                             '~taxi: Squadjoin command for taxi to current TT event instance\n'
                             '~guides: Eggblocking guides for TT\n'
                             '~info: What I am and what I do\n'
                             '~help: This help message\n'
                             '~source: Link to source code of me\n'
                             'Maybe there will be more, most of the OC Staff is able to configure them (see ~adminhelp)')
    bot.write_preset('source', 'My source code: [url]https://gitlab.com/networkjanitor/opencommunity-ts3-bot[/url]')
    bot.write_preset('adminhelp', 'Most of the OC Staff is able to configure commands with the ~set command: \n'
                                  'Format: ~set command_name text \n'
                                  'Example: ~set taxi /sqjoin Triple Trouble Taxi\n'
                                  'Result: \n'
                                  '    User1: ~taxi\n'
                                  '    Link-o-Tron: /sqjoin Triple Trouble Taxi\n\n'
                                  'To remove commands use the ~unset command.')
    # start listening to the notifyevents
    bot.start_listening()
