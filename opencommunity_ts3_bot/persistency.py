from sqlalchemy import Column, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

Base = declarative_base()


class Command(Base):
    __tablename__ = 'command'
    cmd = Column(String(20), primary_key=True)
    text = Column(String(1000), nullable=False)

engine = create_engine('sqlite:///ts3bot-commands.db')

Base.metadata.create_all(engine)

Base.metadata.bind = engine

DBSession = sessionmaker(bind=engine)

session = DBSession()
