OpenCommunity TS3 Bot
=====================

This package provides a ts3 (teamspeak3) chat bot,
which can post a specific text after you called it via special text message.

* can be configured via text messages
* can be called via text messages
* reponds in the same channel as called
* saves configured commands in sqlite database
* special commands can be restricted to certain channel/server groups (not configurable via text message)

Installation
------------

Designed to be run on linux (Debian Jessie i.e.).

Package Only
````````````

#. clone the repository
#. (optional) create virtual env ``python3 -m venv venv``
#. install ``SQLAlchemy`` from pypi
#. install the patched py-ts3 package, either from the wheel included in this repo or from cloning and building https://gitlab.com/networkjanitor/py-ts3

Full Install (including Teamspeak3 Client and systemd)
``````````````````````````````````````````````````````
#. do the steps from `Package Only`_
#. install teamspeak3 client to ~/TeamSpeak3-Client-linux_amd64/
#. run and setup the ts3 client (identities, default server, etc)
#. configure the ts3 bot (apikey, own identity key)
#. copy ts3-bot.service and ts3-client to ~/.config/systemd/user/
#. (optional) test them via ``systemctl start --user ts3-client && systemctl start --user ts3-bot``
#. enable them via ``systemctl enable --user ts3-client && systemctl enable --user ts3-bot``

Host, Environment
`````````````````

Have this running on a Debian Jessie VM w/ LXDE. Using stock LXDE instead of Xvfb because I want to actually see the
TS3 Client and possible incoming chat messages. Also setup would require more work, because you would need to automate
the TS3 Client setup (default server, auto-join channel/server, identity creation, name, read apikey). Could be done by
copying an existing & set up instance.

TODOs
-----

* D O C U M E N T A T I O N (whole bot was written in 10hrs, more to be proof-of-concept than production-ready)
* configuration files
* Tests ? nobody needs tests .. (TODO: tests)

